import java.util.Scanner;

public class LowerTriangle {
    int [][] array=new int[2][2];
    int rows= array.length;
    int cols=array[0].length;
    int count=0;
    public void isSquare(){
        Scanner scanner= new Scanner(System.in);
        System.out.println("Enter the number of rows and columns:");
        int rows = scanner.nextInt();
        int cols = scanner.nextInt();

        if(rows!=cols){
            System.out.println("Matrix should be Square");
        }
        else{
            System.out.println("Lower triangular matrix");
            System.out.println("Enter the elements in the array:");
                  //array[rows][cols]=scanner.nextInt();
            for(int outerIterate=0;outerIterate<rows;outerIterate++) {
                for (int innerIterate = 0; innerIterate < cols; innerIterate++) {
                    array[outerIterate][innerIterate] = scanner.nextInt();
                }
            }
            boolean isLowerTriangle=true;
            for(int outerIterate=0;outerIterate<rows;outerIterate++){
                for (int innerIterate = 0; innerIterate < cols; innerIterate++){
                    if(outerIterate<innerIterate&&array[outerIterate][innerIterate]!=0){
                        isLowerTriangle=false;
                        break;
                    }
                }

            }
            if (isLowerTriangle) {
                System.out.println("yes");
            } else {
                System.out.println("no");
            }
        }
    }
}
